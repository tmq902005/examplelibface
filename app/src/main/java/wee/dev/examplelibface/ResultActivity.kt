package wee.dev.examplelibface

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        initView()
    }

    private fun initView() {
        actResult_image.setImageBitmap(MainActivity.userImage)
        actResult_title.text = "Liveness Result: ${MainActivity.isCheckResult}"

        actResult_btnBack.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onDestroy() {
        MainActivity.userImage?.recycle()
        super.onDestroy()
    }
}
