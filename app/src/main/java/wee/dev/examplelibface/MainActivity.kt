package wee.dev.examplelibface

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import wee.dev.libface.base.LiveNessCheckData
import wee.dev.libface.control.EkycFaceView

class MainActivity : AppCompatActivity(), EkycFaceView.EkycFaceViewCallBack {

    companion object{
        const val REQUEST_PERMISSION_CODE = 100

        const val CAMERA_PERMISSION = android.Manifest.permission.CAMERA

        var userImage: Bitmap? = null

        var userVideoPath: String = ""

        var isCheckResult: Boolean = false
    }

    private var isPermission = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissionCamera()
    }

    private fun initControl(){
        actMain_ekycFaceControl.initLibrary(this)
    }

    private fun checkPermissionCamera(){
        isPermission = checkSelfPermission(CAMERA_PERMISSION) == PackageManager.PERMISSION_GRANTED
        if(!isPermission) {
            requestPermissions(arrayOf(CAMERA_PERMISSION), REQUEST_PERMISSION_CODE)
        }else{
            initControl()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode== REQUEST_PERMISSION_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) initControl()
        }
    }

    private fun actionDone(bitmap: Bitmap?, videoPath: String, isLiveness: Boolean){
        val intent = Intent(this@MainActivity,ResultActivity::class.java)
        isCheckResult = isLiveness
        userVideoPath = videoPath
        userImage = bitmap
        startActivity(intent)
        finish()
    }

    override fun onPause() {
        super.onPause()
        actMain_ekycFaceControl.onPauseLibrary()
    }

    override fun onResume() {
        super.onResume()
        actMain_ekycFaceControl.onResumeLibrary()
    }

    override fun onDestroy() {
        super.onDestroy()
        actMain_ekycFaceControl.onDestroyLibrary()
    }

    override fun onComplete(data: LiveNessCheckData) {

    }
}
